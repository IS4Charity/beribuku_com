﻿# README #

# Development Guidance #
## Pre-requisites ##
* GIT
* Python2.x.y (please use latest version)
* linux environment (it's just our recommendation, because in production we are using linux server, but you have freedom to use another environment)

Development flow of this project follow git flow which proposed  by Vincent Driesse, [see here](http://nvie.com/posts/a-successful-git-branching-model/m)


To contribute in this project, you could follow below steps:

## Cloning the Project ##
Clone project Beribuku_com di 
git clone -b development-v2.0 https://gitlab.com/IS4Charity/beribuku_com.git

## Installation ##
1. Create and activate python virtual environment
```
virtualenv myenv && source myenv/bin/activate
```

2. Install dependencies
```
sudo apt-get install python-dev libpq-dev libxml2-dev libxslt-dev
```

3. Install python-packages
```
pip install <your-repo-path>/req.txt
```
Note : before install python-packages, please make sure you've activate python 
virtual environment by using command as follow:
```
source <your-repo-path>/bin/activate 
```

## Running the Project ##
```
#!python

python manage.py runserver
```
Note : by default the project will running in port 8000


## Directory structure##
To Be Detailed


## Contact##
If you have any curiosity about this project you could contact [moh.afifun46@gmail.com](mailto:moh.afifun46@gmail.com) 
