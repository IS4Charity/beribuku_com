from django.db import models
from django.core.validators import RegexValidator
from django import forms
from functools import partial
from django.forms.extras.widgets import SelectDateWidget

from django.utils import timezone

import datetime

# Create your models here.

DateInput = partial(forms.DateInput, {'class': 'datepicker'})

class Nama(models.Model):
	alphanumeric = RegexValidator(r'^[a-zA-Z]*$', 'Only alphanumeric characters are allowed.')
	namaDepan = models.CharField(max_length=100, validators=[alphanumeric])
	namaBelakang = models.CharField(max_length=100)

class Akun(models.Model):
	
	username = models.CharField(max_length=100)
	password = models.CharField(max_length=100)

class Program(models.Model):
	nama = models.CharField(max_length=200)
	deskripsi = models.TextField()
	target = models.CharField(max_length=200)
	partner = models.CharField(max_length=200)
	logoPartner = models.FileField(upload_to='static/images/logo')

	def __str__(self):
		return '%s. %s' % (self.id, self.nama)

	@property
	def foto_first(self):
		fotos = self.fotoprogram_set.all()
		if len(fotos) > 0:
			return fotos[0]
		else:
			return None

class FotoProgram(models.Model):
	program = models.ForeignKey('Program')
	gambar = models.FileField(upload_to='static/images/proyek')
	caption = models.CharField(max_length=250, default="")

class Barang(models.Model):
	program = models.ForeignKey('Program')
	nama = models.CharField(max_length=200)
	jumlah = models.CharField(max_length=100, null=True)

class Rekening(models.Model):
	nama = models.CharField(max_length=50)
	noRekening = models.CharField(max_length=50)
	bank = models.CharField(max_length=200)

class Donasi(models.Model):
	program = models.ForeignKey('Program')
	rekening = models.ForeignKey('Rekening')
	deadline = models.DateField()

	def belum_lewat_deadline(self):
		return self.deadline >= datetime.date.today()
 
class Donatur(models.Model):
	program = models.ForeignKey('Program')
	nama = models.CharField(max_length=50)
	email = models.CharField(max_length=50, null=True)
	nohp = models.CharField(max_length=15, null=True)

class Tim(models.Model):
	nama = models.CharField(max_length=50)
	email = models.CharField(max_length=50)
	jabatan = models.CharField(max_length=200)
	foto = models.FileField(upload_to='beribuku_app/static/images/tim')

class About(models.Model):
	judul = models.CharField(max_length=100)
	deskripsi = models.CharField(max_length=1200)

class Laporan(models.Model):
	keterangan = models.TextField(null=True)
	program = models.ForeignKey('Program')

class Laporan_temp(models.Model):
	filecontent = models.FileField(upload_to='static/files')

class Laporan_item(models.Model):
	laporan = models.ForeignKey('Laporan')
	keterangan = models.CharField(max_length=1000)
	nominal = models.IntegerField()
	isDebit = models.BooleanField()
	isKredit = models.BooleanField()

class Test(models.Model):
	a = models.IntegerField()

class Donasi_donatur(models.Model):
	donatur = models.ForeignKey('Donatur')
	tanggal = models.DateField()
	konfirmasi = models.BooleanField()
	via = models.CharField(max_length=50, null=True)

class Donasi_uang(models.Model):
	jenis = 'uang'
	donasi_donatur = models.ForeignKey('Donasi_donatur')
	bank = models.CharField(max_length=50, null=True)
	jumlah_uang = models.IntegerField()
	bukti_transfer = models.FileField(upload_to='static/foto/bukti_transfer')

class Donasi_buku(models.Model):
	jenis = 'buku'
	donasi_donatur = models.ForeignKey('Donasi_donatur')
	keterangan = models.CharField(max_length=500, null=True)

class FileMailTemp(models.Model):
	fileUpload = models.FileField(upload_to='static/files')

class Subscriber(models.Model):
	date = models.DateTimeField()
	phone = models.IntegerField()
	email = models.EmailField()
