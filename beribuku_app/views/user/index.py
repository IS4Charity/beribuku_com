import itertools
import json

from beribuku_app.views.admin.login import auth
from django.core.serializers.json import DjangoJSONEncoder
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils import timezone

from beribuku_app.models import *
from beribuku_app.views import validator
from .utils import get_latest_program


def handler404(request):
    response = render_to_response('404.html', {},
                                  context_instance=RequestContext(request))
    response.status_code = 404
    return response


def index(request):
    # template = loader.get_template("beribuku_app/index.html")
    # return HttpResponse(template.render())
    try:
        program = Program.objects.all().order_by('-id')[0]
        data = {'program': program}
    except:
        data = {}
    return HttpResponseRedirect('/home/','flatpage', data)


def home(request):
    try:
        program = Program.objects.all().order_by('-id')[0]
        latest_program = get_latest_program()

        def mygrouper(n, iterable):
            args = [iter(iterable)] * n
            return ([e for e in t if e != None] for t in itertools.izip_longest(*args))

        data = {'latest_program' : latest_program, 'programs': Program.objects.all().order_by('id')[:9],  'foto_programs': mygrouper(4,list(FotoProgram.objects.all())), 'login': auth(request), 'title':"Home", 'prgnow': program}
    except Exception as e:
	print e
        data = {}
    return render(request, '2017_design/index.html', data)

def about(request):
    data = {'program': Program.objects.all(), 'login': auth(request), 'title':"About", "aktif_about":"active"}
    return render(request, '2017_design/about.html', data)

def tim(request):
    data = {'program': Program.objects.all(), 'tim' : Tim.objects.all(), 'login': auth(request), 'title':"Team", "aktif_tim":"active"}
    return render(request, 'tim2.html', data)

def contact(request):
    data = {'program': Program.objects.all(), 'login': auth(request), 'title':"Contact", "aktif_login":"active"}
    return render(request, 'contact.html', data)

def addDonatur(request):

    if request.method == 'POST':
        programId   = validator.escape(request.POST.get('program'))
        program     = get_object_or_404(Program, pk=programId)

        nama        = validator.escape(request.POST.get('nama'))
        email       = validator.escape(request.POST.get('email'))
        nohp        = validator.escape(request.POST.get('nohp'))

        try:
            bukti   = request.FILES['buktitransfer']
        except:
            bukti   = None

        vNama       = not validator.is_empty(nama)
        vEmail      = not validator.is_empty(email) and validator.is_email(email)
        vNoHp       = not validator.is_empty(nohp) and validator.is_number(nohp)
        vBukti      = not validator.is_empty(bukti) and validator.is_image(bukti.content_type)

        valid       = vNama and vEmail and vBukti and vNoHp

        if valid:

            donatur = Donatur(program=program, nama=nama, email=email, bukti=bukti)
            donatur.save()

        else:
                err_msg = []

                if not vNama:
                    err_msg.append("Bagian 'Nama' tidak boleh kosong")

                if not vEmail:
                    err_msg.append("Bagian 'Email' harus diisi dengan email yang valid")

                if not vNoHp:
                    err_msg.append("Bagian 'No. Handphone' harus diisi dengan no. HP yang valid")

                if not vBukti:
                    err_msg.append("Bagian 'Bukti Transfer' harus diisi dengan file gambar")

                data = {
                    'program' : Program.objects.all(), 
                    'login': auth(request),
                    'nama' : nama,
                    'email' : email,
                    'nohp' : nohp,
                    'error' : err_msg,
                    'title':"Konfirmasi Donatur"
                }

                return render(request, 'newDonatur.html', data) 

        return render(request, 'berhasil.html', {'program' : Program.objects.all(), 'login': auth(request), 'nama' : nama, 'title': "Berhasil menambah donatur"})
    else:
        return render(request, 'newDonatur.html', {'program' : Program.objects.all(), 'login': auth(request), 'title': "Konfirmasi Donatur"})


def subscribe(request):
    if request.method == 'POST' and request.is_ajax():
        respons = {'state': 0}
        result = request.POST
        phone = result['phone']
        email = result['email']

        subscriber = list(Subscriber.objects.filter(Q(phone=phone)|Q(email=email)))

        try:
            if len(subscriber) > 0:
                respons.update({'state' : 2 })
            else:
                now = timezone.now()
                subscriber = Subscriber(date=now,phone=phone, email=email)
                subscriber.save()
                respons.update({'state':1})
        except Exception as e:
            pass

        return HttpResponse(json.dumps(respons, cls=DjangoJSONEncoder), content_type="application/json")


def donasi(request):
    return render(request, '2017_design/donasi.html', {'aktif_donasi': 'active'})


def blog(request):
    return render(request, '2017_design/blog.html', {'aktif_blog': 'active', 'team': Tim.objects.all()})

def authenticate(request):
    if request.user.groups.filter(name='administrator'):
        return HttpResponseRedirect(reverse('admin.home'))
    else:
        return HttpResponseRedirect(reverse('index'))

