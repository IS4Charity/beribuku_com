from django.shortcuts import render, get_object_or_404

from beribuku_app.models import *
from beribuku_app.views.admin.login import auth


def index(request):
    donasi = Donasi.objects.all()
    foto   = FotoProgram.objects.all()
    donasi_selected = None


    for d in donasi :
        if d.belum_lewat_deadline():
            donasi_selected = d

    if donasi_selected == None:
        proyek_selected = None
        title           = 'Proyek Saat Ini'
    else:
        proyek_selected = donasi_selected.program
        title           = proyek_selected.nama

    data = {'program': Program.objects.all().order_by('-id'),'program_selected':proyek_selected, 'fotoProgram': foto, 'login': auth(request), 'title':title, "aktif_proyek":"active"}
    return render(request, 'proyek.html', data)

def all(request):
    try:
        data = {'programs': Program.objects.all().order_by('id')[:6], 'fotoProgram': FotoProgram.objects.all(), 'login': auth(request), 'title':"Proyek"}
    except:
        data = {}
    return render(request, '2017_design/proyek-list.html', data)


def detil(request, id):
    if request.method == "GET":
        data = {}
        try:
            programId 				= id
            proyek_selected 		= get_object_or_404(Program, pk=programId)
        except Exception as e:
            proyek_selected = None
        try:
            proyek 					= Program.objects.all().order_by("-id")
        except Exception as e:
            proyek = None

        proyek_other 			= []

        try:
            fotoProgram_selected 	= FotoProgram.objects.filter(program=programId)
        except Exception as e:
            fotoProgram_selected = None

        try:
            fotoProgram	= FotoProgram.objects.all()[:20]
        except Exception as e:
            fotoProgram = None

        try:
            donasi_selected 		= Donasi.objects.filter(program=programId)
        except Exception as e:
            donasi_selected = None

        try:
            laporan = Laporan.objects.get(program=proyek_selected)
        except Exception as e:
            laporan = None

        counter					= 2
        for p in proyek:
            if not p == proyek_selected and counter > 0:
                proyek_other.append(p)
                counter = counter - 1

        if proyek_selected:
            data = {'proyek_selected': proyek_selected, 'proyek_other': proyek_other,
                    'fotoProgram_selected': fotoProgram_selected,'fotoProgram': fotoProgram,
                    'donasi_selected': donasi_selected,
                    'title': proyek_selected.nama, 'laporan': laporan}
        else:
            data = {}

        return render(request, '2017_design/detilProyek.html', data)
