__author__ = 'afifun'

from rest_framework import serializers
from rest_framework import viewsets

from beribuku_app.models import FotoProgram, Donasi


class FotoProgramSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = FotoProgram
        fields = ('gambar', 'caption')


class FotoProgramViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Foto Program to be viewed.
    """
    queryset = FotoProgram.objects.all()
    serializer_class = FotoProgramSerializer


def get_latest_program():
    donasi = Donasi.objects.all()
    donasi_selected = None


    for d in donasi :
        if d.belum_lewat_deadline():
            donasi_selected = d

    if donasi_selected == None:
        proyek_selected = None
        title           = 'Proyek Saat Ini'
    else:
        proyek_selected = donasi_selected.program
        title           = proyek_selected.nama

    return proyek_selected
