import re, html

def escape(s):
	return html.escape(s).strip()

def escape_list(l):
	escaped = []
	print(len(l))
	for i in range(len(l)):
		escaped.append(html.escape(l[i]).strip())

	return escaped

def is_empty(s):
	return s == None or s == ''

def is_number(s):
	try:
		float(s)
		return True
	except ValueError:
		return False

def is_email(s):
	try:
		if re.match(r"[^@]+@[^@]+\.[^@]+", s):
			return True
		else:
			return False
	except:
		return False

def is_image(f):
	try:
		print(f)
		mime =  f.split('/')[0] == 'image'
		print(mime)
		ext = f.split('/')[1] == 'jpg' or f.split('/')[1] == 'png' or f.split('/')[1] == 'gif' or f.split('/')[1] == 'jpeg'
		print(ext)
		return mime and ext
	except Exception as e:
		print(e)
		return False
