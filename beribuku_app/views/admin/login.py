from django.shortcuts import render, redirect
from beribuku_app.models import *
from django.shortcuts import render, redirect

from beribuku_app.models import *


def index(request):
	
	context_dict = {'aktif_login': 'active','title': 'Login'}
	if (request.POST):
		try:
			user = Akun.objects.get(username = request.POST['username'])
			if user.password == request.POST['password'] :
				request.session['userId'] = user.id
				return redirect('/admin/home/')
			else:
				data = {'aktif_login': 'active','title': 'Login','msg': 'username/password tidak cocok'}
				return render(request, 'admin/login2.html', data)

		except Akun.DoesNotExist:
			data = {'aktif_login': 'active','title': 'Login','msg': 'username/password tidak cocok'}
			return render(request, 'admin/login2.html', data)

	else :
		authUser = auth(request)
		if authUser:
			return redirect('/admin/home/')
		else :
			return render(request, 'admin/login2.html', context_dict)


def logout(request):
	context_dict = {}
	del request.session['userId']
	request.session.modified = True
	return redirect('/admin/login/')

def auth(request):
	return request.user.groups.filter(name="administrator").count() > 0
