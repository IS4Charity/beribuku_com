import os
from beribuku_app.views.admin.login import *
from django.shortcuts import get_object_or_404

from django.contrib.auth.decorators import login_required

from beribuku_app.views import validator

@login_required
def index(request):

    data = {'tim' : Tim.objects.all(), 'rekening' : Rekening.objects.all(), 'about' : About.objects.all(),
                    'admin_master_active': 'active'}

    return render(request, 'admin/master/master.html', data)

def addNewTim(request):

    data = {'admin_master_active': 'active'}

    if request.method == 'POST':
        
        nama    = validator.escape(request.POST.get('nama'))
        posisi  = validator.escape(request.POST.get('posisi'))
        email   = validator.escape(request.POST.get('email'))

        try:
            foto    = request.FILES['foto']
        except:
            foto    = None

        vNama   = not validator.is_empty(nama)
        vPosisi = not validator.is_empty(posisi)
        vEmail  = not validator.is_empty(email) and validator.is_email(email)
        vFoto   = not validator.is_empty(foto) and validator.is_image(foto.content_type)

        valid   = vNama and vPosisi and vEmail and vFoto

        if valid:

            tim = Tim(nama=nama, email=email, jabatan=posisi, foto=foto)
            tim.save()

            return redirect('/admin/master/')
        else:
            err_msg = []

            if not vNama:
                err_msg.append("Bagian 'Nama' tidak boleh kosong")
            if not vPosisi:
                err_msg.append("Bagian 'Jabatan' tidak boleh kosong")
            if not vEmail:
                err_msg.append("Bagian 'Email' harus diisi dengan email yang valid")
            if not vFoto:
                err_msg.append("Bagian 'Foto' harus diisi dengan file gambar")

            data = {
                'nama'  : nama,
                'posisi': posisi,
                'email' : email,
                'error' : err_msg
            }

            return render(request, 'admin/master/new.html', data)

    else :
        return render(request, 'admin/master/new.html', data)

def editTim(request):
    data = {'admin_master_active': 'active'}
    err_msg = []
    if request.method == 'GET':
        tim_id = request.GET.get('id')
        tim = get_object_or_404(Tim, id=tim_id)
        print(tim.foto)
        data = {'tim' : tim, 'admin_master_active': 'active'}

    elif request.method == 'POST':
        tim_id = request.GET.get('id')
        tim = get_object_or_404(Tim, id=tim_id)

        nama    = validator.escape(request.POST.get('nama'))
        posisi  = validator.escape(request.POST.get('posisi'))
        email   = validator.escape(request.POST.get('email'))

        try:
            foto    = request.FILES['foto']
        except:
            foto    = None

        vNama   = not validator.is_empty(nama)
        vPosisi = not validator.is_empty(posisi)
        vEmail  = not validator.is_empty(email) and validator.is_email(email)
        vFoto   = not validator.is_empty(foto)

        valid   = vNama and vPosisi and vEmail

        if valid:
            tim.nama = nama
            tim.jabatan = posisi
            tim.email = email

            if vFoto:
                if validator.is_image(foto.content_type):
                    try:
		        os.remove(str(tim.foto))
                    except:
			pass

		    tim.foto = foto
                else:
                    err_msg.append("Bagian 'Foto' harus diisi dengan file gambar")
                    data = {
                        'tim'  : tim,
                        'error' : err_msg
                    }
                    return render(request, 'admin/master/new.html', data)
            tim.save()

            return redirect('/admin/master/')
        else:

            if not vNama:
                err_msg.append("Bagian 'Nama' tidak boleh kosong")
            if not vPosisi:
                err_msg.append("Bagian 'Jabatan' tidak boleh kosong")
            if not vEmail:
                err_msg.append("Bagian 'Email' harus diisi dengan email yang valid")
            if not vFoto:
                err_msg.append("Bagian 'Foto' harus diisi dengan file gambar")

            data = {
                'nama'  : nama,
                'posisi': posisi,
                'email' : email,
                'error' : err_msg,
                'admin_master_active': 'active'
            }

            return render(request, 'admin/master/new.html', data)

    return render(request, 'admin/master/new.html', data)

def deleteTim(request):
    
    data = {}

    if auth(request):
        if request.method == 'GET':
            timId = request.GET.get('timId')

            tim = get_object_or_404(Tim, pk=timId)
            tim.delete()

            return redirect('/admin/master/')
    else:

        return redirect('/admin/login/')

def addNewRekening(request):

    data = {'admin_master_active': 'active'}

    if request.method == 'POST':
        
        nama            = validator.escape(request.POST.get('nama'))
        noRekening      = validator.escape(request.POST.get('nomor'))
        bank            = validator.escape(request.POST.get('bank'))

        vNama           = not validator.is_empty(nama) 
        vRek            = not validator.is_empty(noRekening)
        vBank           = not validator.is_empty(bank)

        valid = vNama and vRek and vBank

        if valid:
            rekening = Rekening(nama=nama, noRekening=noRekening, bank=bank)
            rekening.save()

            return redirect('/admin/master/')
        else:
            err_msg = []

            if not vNama:
                err_msg.append("Bagian 'Nama' tidak boleh kosong")
            if not vRek:
                err_msg.append("Bagian 'Nomor Rekening' tidak boleh kosong")
            if not vBank:
                err_msg.append("Bagian 'Nama Bank' tidak boleh kosong")

            data = {
                'nama'  : nama,
                'rek': posisi,
                'bank' : email,
                'error' : err_msg,
                'admin_master_active': 'active'
            }

            render(request, 'admin/master/rekening.html', data)
        
    else :
        return render(request, 'admin/master/rekening.html', data)

def editRekening(request):
    data = {'admin_master_active': 'active'}

    if request.method == 'GET':
        rekening_id = request.GET.get('id')
        rekening = get_object_or_404(Rekening, id=rekening_id)
        data = {'rekening' : rekening, 'admin_master_active': 'active'}

    elif request.method == 'POST':

        rekening_id = request.GET.get('id')
        rekening = get_object_or_404(Rekening, id=rekening_id)

        nama            = validator.escape(request.POST.get('nama'))
        noRekening      = validator.escape(request.POST.get('nomor'))
        bank            = validator.escape(request.POST.get('bank'))

        vNama           = not validator.is_empty(nama) 
        vRek            = not validator.is_empty(noRekening)
        vBank           = not validator.is_empty(bank)

        valid = vNama and vRek and vBank

        if valid:
            rekening.nama = nama
            rekening.noRekening = noRekening
            rekening.bank = bank
            rekening.save()

            return redirect('/admin/master/')
        else:
            err_msg = []

            if not vNama:
                err_msg.append("Bagian 'Nama' tidak boleh kosong")
            if not vRek:
                err_msg.append("Bagian 'Nomor Rekening' tidak boleh kosong")
            if not vBank:
                err_msg.append("Bagian 'Nama Bank' tidak boleh kosong")

            data = {
                'nama'  : nama,
                'rek': posisi,
                'bank' : email,
                'error' : err_msg,
                'admin_master_active': 'active'
            }

            return render(request, 'admin/master/rekening.html', data)

    return render(request, 'admin/master/rekening.html', data)

def deleteRekening(request):
    if auth(request):
        if request.method == 'GET':
            rekeningId = request.GET.get('rekeningId')

            rek = get_object_or_404(Rekening, pk=rekeningId)
            rek.delete()

            return redirect('/admin/master/')
    else:
        return redirect('/admin/login/')

def addNewAbout(request):

    data = {}

    if request.method == 'POST':
        
        nama = request.POST.get('nama')
        deskripsi = request.POST.get('deskripsi')

        about = About(nama=nama, deskripsi=deskripsi)
        about.save()

        return redirect('/admin/master/')
    else :
        return render(request, 'admin/master/about.html', data)

def deleteAbout(request):
    
    data = {}

    if auth(request):
        if request.method == 'GET':
            aboutId = request.GET.get('aboutId')
            try:
                About.objects.filter(id=aboutId).delete()
                return redirect('/admin/master/')
            except:
                return redirect('/admin/master/')
    else:

        return redirect('/admin/login/')
