from beribuku_app.views.admin.login import *
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import login_required
from docx import Document
from docx.shared import Inches

from beribuku_app.views import validator


@login_required
def index(request, alert=False, alert_msg=""):
    try:
        data = {
            'program': Program.objects.all().order_by('-id'),
            'donasi': Donasi.objects.all(),
            'rekening': Rekening.objects.all(),
            'alert_success': alert,
            'alert_msg' : alert_msg,
            'admin_proyek_active' : 'active'
        }
    except Exception as e:
        print(e)
        return render(request, 'admin/program/program.html')

    return render(request, 'admin/program/program.html', data)

@login_required
def viewProgram(request):

    data = {'admin_proyek_active' : 'active'}

    if request.method == 'GET':
        programId = request.GET.get('programId')
        program = get_object_or_404(Program, pk=programId)

        try:
            laporan = Laporan.objects.get(program=program)
        except:
            laporan = None

        if laporan:
            laporan_item = Laporan_item.objects.filter(laporan=laporan)
            jumlah_debit = 0
            jumlah_kredit = 0
            for a in laporan_item:
                if a.isDebit:
                    jumlah_debit = jumlah_debit + a.nominal
                if a.isKredit:
                    jumlah_kredit = jumlah_kredit + a.nominal
            saldo = jumlah_debit - jumlah_kredit

            data = {
                'program': program,
                'laporan' : laporan_item,
                'laporan_realisasi': laporan.keterangan != None,
                'saldo': saldo,
                'jumlah_kredit': jumlah_kredit,
                'jumlah_debit': jumlah_debit,
                'admin_proyek_active' : 'active'
            }
        else:
            data = {
                'program': program,
                'laporan' : None,
                'admin_proyek_active' : 'active'
            }

        return render(request, 'admin/program/viewProgram.html', data)

def editProgram(request):

    data = {'admin_proyek_active' : 'active'}
    programId=None
    if request.method == 'GET':
        programId = request.GET.get('programId')
        program = get_object_or_404(Program, pk=programId)

        rekening = Rekening.objects.all()
        donasi = Donasi.objects.filter(program=program)
        barang = Barang.objects.filter(program=program)
        fotoProgram = FotoProgram.objects.filter(program=program)

        listRekeningDonasi = []
        for d in donasi:
            listRekeningDonasi.append(d.rekening)

        data = {'program': program,
                'donasi': donasi,
                'rekening': rekening,
                'fotoProgram': fotoProgram,
                'listRekeningDonasi': listRekeningDonasi,
                'barang': barang,
                'admin_proyek_active' : 'active'}

        return render(request, 'admin/program/editProgram.html', data)

    elif request.method == 'POST':

        programId = request.GET.get('programId')
        p = get_object_or_404(Program, pk=programId)
        logoPartnerUnchanged=False

        nama            = validator.escape(request.POST.get('nama'))
        deskripsi       = validator.escape(request.POST.get('deskripsi'))
        target          = validator.escape(request.POST.get('target'))
        partner         = validator.escape(request.POST.get('partner'))
        deadline        = validator.escape(request.POST.get('deadline'))

        namaBarang      = validator.escape_list(request.POST.getlist('barang'))
        jumlahBarang    = validator.escape_list(request.POST.getlist('jumlahBarang'))
        rekening        = validator.escape_list(request.POST.getlist('rekening'))
        removeGambar    = validator.escape_list(request.POST.getlist('remove-gambar'))
        gambarcaption   = validator.escape_list(request.POST.getlist('gambarcaption'))

        editcaption     = []
        for gambar in p.fotoprogram_set.all():
            editcaption.append([gambar.id, validator.escape(request.POST.get('caption-' + str(gambar.id)))])

        try:
            logoPartner     = request.FILES['logoPartner']
        except:
            logoPartner     = None

        if logoPartner == None:
            logoPartnerUnchanged = True

        try:
            gambarProgram   = request.FILES.getlist('gambar')
        except:
            gambarProgram   = []

        ######################################################################

        vNama               = not validator.is_empty(nama)
        vDesc               = not validator.is_empty(deskripsi)
        vTarget             = not validator.is_empty(target)
        vDeadline           = not validator.is_empty(deadline)

        if validator.is_empty(logoPartner):
            vLogoPartner = True
        else:
            vLogoPartner = logoPartnerUnchanged or (validator.is_image(logoPartner.content_type))

        vGambar             = True
        vNBarang            = True
        vRekening           = True

        if not namaBarang:
            vNBarang        = False
        if not rekening:
            vRekening       = False

        for g in gambarProgram:
            if not validator.is_image(g.content_type):
                vGambar     = False
                break;

        valid = vNama and vDesc and vTarget and vLogoPartner and vDeadline and vGambar and vNBarang and vRekening

        ######################################################################

        if valid:
            p.nama          = nama
            p.deskripsi     = deskripsi
            p.target        = target
            p.partner       = partner

            if not logoPartnerUnchanged:
                p.logoPartner   = logoPartner

            p.save()

            for i,gambar in enumerate(gambarProgram):
                fotoProgram = FotoProgram(program=p, gambar=gambar, caption=gambarcaption[i])
                fotoProgram.save()

            for edit in editcaption:
                gambar = get_object_or_404(FotoProgram, pk=edit[0])
                gambar.caption = edit[1]
                gambar.save()

            # fotoProgram = FotoProgram.objects.filter(program=p)
            for gId in removeGambar:
                gambar = get_object_or_404(FotoProgram, pk=gId)
                gambar.delete()

            barang = Barang.objects.filter(program=p)
            for b in barang:
                b.delete()

            donasi = Donasi.objects.filter(program=p)
            for d in donasi:
                d.delete()

            for k,i in enumerate(namaBarang):
                jumlah = jumlahBarang[k]
                barang = Barang(program=p, nama=i, jumlah=jumlah)
                barang.save()

            for rek in rekening:
                rekening = Rekening.objects.get(id=rek)
                d = Donasi(program=p, rekening=rekening, deadline=deadline)
                d.save()

            alert_msg = "Berhasil mengubah program " + p.nama
            return redirect(reverse('admin.program', args=(True, alert_msg,)))

        else:

            err_msg = []

            if not vNama:
                err_msg.append("Bagian 'Nama' tidak boleh kosong")
            if not vDesc:
                err_msg.append("Bagian 'Deskripsi' tidak boleh kosong")
            if not vTarget:
                err_msg.append("Bagian 'Target' tidak boleh kosong")
            if not vLogoPartner:
                err_msg.append("Bagian 'Logo Partner' harus diisi dengan file gambar")
            if not vDeadline:
                err_msg.append("Bagian 'Deadline' tidak boleh kosong")
            if not vGambar:
                err_msg.append("Bagian 'Gambar' harus diisi dengan file gambar")
            if not vNBarang:
                err_msg.append("Bagian 'Nama Kebutuhan' tidak boleh kosong")
            if not vRekening:
                err_msg.append("Harus terdapat minimal satu rekening")

            program         = p

            rekening        = Rekening.objects.all()
            donasi          = Donasi.objects.filter(program=program)
            barang          = Barang.objects.filter(program=program)
            fotoProgram     = FotoProgram.objects.filter(program=program)

            listRekeningDonasi = []
            for d in donasi:
                listRekeningDonasi.append(d.rekening)

            data = {
                'program'           : program,
                'donasi'            : donasi,
                'rekening'          : rekening,
                'fotoProgram'       : fotoProgram,
                'listRekeningDonasi': listRekeningDonasi,
                'barang'            : barang,
                'error'             : err_msg,
                'admin_proyek_active' : 'active'
            }

            return render(request, 'admin/program/editProgram.html', data)
    else:
        pass


def addNewProgram(request):

    data = {}

    if request.method == 'POST':

        nama            = validator.escape(request.POST.get('nama'))
        deskripsi       = validator.escape(request.POST.get('deskripsi'))
        target          = validator.escape(request.POST.get('target'))
        partner         = validator.escape(request.POST.get('partner'))
        deadline        = validator.escape(request.POST.get('deadline'))

        namaBarang      = validator.escape_list(request.POST.getlist('barang'))
        jumlahBarang    = validator.escape_list(request.POST.getlist('jumlahBarang'))
        rekening        = validator.escape_list(request.POST.getlist('rekening'))
        gambarcaption   = validator.escape_list(request.POST.getlist('gambarcaption'))

        try:
            logoPartner     = request.FILES['logoPartner']
        except:
            logoPartner     = None

        try:
            gambarProgram   = request.FILES.getlist('gambar')
        except:
            gambarProgram   = []

        ######################################################################

        vNama           = not validator.is_empty(nama)
        vDesc           = not validator.is_empty(deskripsi)
        vTarget         = not validator.is_empty(target)
        vDeadline       = not validator.is_empty(deadline)
        vLogoPartner    = logoPartner == None or validator.is_image(logoPartner.content_type)

        vGambar         = True
        vNBarang        = True
        vRekening       = True

        if not gambarProgram:
            vGambar = False
        if not namaBarang:
            vNBarang = False
        if not rekening:
            vRekening = False

        for g in gambarProgram:
            if not validator.is_image(g.content_type):
                vGambar = False
                break;

        valid = vNama and vDesc and vTarget and vLogoPartner and vDeadline and vGambar and vNBarang and vRekening

        ######################################################################

        if valid:
            program = Program(nama=nama, deskripsi=deskripsi, target=target, partner=partner, logoPartner=logoPartner)
            program.save()

            for i,gambar in enumerate(gambarProgram):
                fotoProgram = FotoProgram(program=program, gambar=gambar, caption=gambarcaption[i])
                fotoProgram.save()

            for rek in rekening:
                rekening = Rekening.objects.get(id=rek)
                donasi = Donasi(program=program, rekening=rekening, deadline=deadline)
                donasi.save()

            for k,i in enumerate(namaBarang):
                jumlah = jumlahBarang[k]

                if not validator.is_number(jumlah):
                    jumlah = None

                barang = Barang(program=program, nama=i, jumlah=jumlah)
                barang.save()

            laporan = Laporan(program=program)
            laporan.save()

            alert_msg = "Berhasil menambahkan program " + nama
            return redirect(reverse('admin.program', args=(True, alert_msg,)))
        else:
                err_msg = []

                if not vNama:
                    err_msg.append("Bagian 'Nama' tidak boleh kosong")
                if not vDesc:
                    err_msg.append("Bagian 'Deskripsi' tidak boleh kosong")
                if not vTarget:
                    err_msg.append("Bagian 'Target' tidak boleh kosong")
                if not vLogoPartner:
                    err_msg.append("Bagian 'Logo Partner' harus diisi dengan file gambar")
                if not vDeadline:
                    err_msg.append("Bagian 'Deadline' tidak boleh kosong")
                if not vGambar:
                    err_msg.append("Bagian 'Gambar' harus diisi dengan file gambar")
                if not vNBarang:
                    err_msg.append("Bagian 'Nama Kebutuhan' tidak boleh kosong")
                if not vRekening:
                    err_msg.append("Harus terdapat minimal satu rekening")

                data = {
                    'rekening': Rekening.objects.all(),
                    'error' : err_msg,
                    'admin_proyek_active' : 'active'
                }

                return render(request, 'admin/program/newProgram.html', data)
    else:
        data = {'rekening': Rekening.objects.all(), 'admin_proyek_active' : 'active'}
        return render(request, 'admin/program/newProgram.html', data)

def deleteProgram(request):

    data = {}

    if auth(request):
        if request.method == 'GET':
            programId = request.GET.get('programId')
            try:
                p = get_object_or_404(Program, pk=programId)
                alert_msg = 'Berhasil menghapus program ' + p.nama
                p.delete()

                return redirect(reverse('admin.program', args=(True, alert_msg,)))

            except Exception as e:
                #Program.objects.filter().delete()
                print(e)
                return redirect('/admin/program/')
    else:

        return redirect('/admin/login/')

def downloadExcelDonatur(request, programId):

    if auth(request):
        if request.method == 'GET':

            program     = get_object_or_404(Program, pk=programId)
            setDonatur  = program.donatur_set.all()

            """     SET FONT AND STYLE      """
            response = HttpResponse(content_type='application/ms-excel')
            response['Content-Disposition'] = 'attachment; filename="donatur-' + program.nama + '.xls"'

            alignmentCenter = xlwt.Alignment()
            alignmentCenter.horz = xlwt.Alignment.HORZ_CENTER
            alignmentCenter.vert = xlwt.Alignment.VERT_CENTER

            pattern = xlwt.Pattern()
            pattern.pattern = xlwt.Pattern.SOLID_PATTERN
            pattern.pattern_fore_colour = xlwt.Style.colour_map['dark_blue']

            fontMain                = xlwt.Font()
            fontMain.name           = 'Calibri'

            fontHeader              = xlwt.Font()
            fontHeader.name         = 'Calibri'
            fontHeader.height       = 11 * 20

            fontTitle               = xlwt.Font()
            fontTitle.name          = 'Calibri'
            fontTitle.bold          = True
            fontTitle.height        = 16 * 20           # 1 twips = 1/20 pt

            styleMainLeft           = xlwt.easyxf('font: name Calibri; border : left thick, right thin')
            styleMainCenter         = xlwt.easyxf('font: name Calibri; border : left thin, right thin')
            styleMainLeftHP         = xlwt.easyxf('font: name Calibri; border : left thin')
            styleMainRightHP        = xlwt.easyxf('font: name Calibri; border : right thick')
            styleMainLeftBottom     = xlwt.easyxf('font: name Calibri; border : left thick, right thin, bottom thick')
            styleMainCenterBottom   = xlwt.easyxf('font: name Calibri; border : left thin, right thin, bottom thick')
            styleMainLeftHPBottom   = xlwt.easyxf('font: name Calibri; border : left thin, bottom thick')
            styleMainRightBottom    = xlwt.easyxf('font: name Calibri; border : right thick, bottom thick')

            styleHeaderLeft         = xlwt.easyxf('font: name Calibri, height 220; align: vertical center, horizontal center; pattern: pattern solid, fore_colour dark_blue; border: left thick, top thick, bottom thick, right thin')
            styleHeaderCenter       = xlwt.easyxf('font: name Calibri, height 220; align: vertical center, horizontal center; pattern: pattern solid, fore_colour dark_blue; border: left thin, top thick, bottom thick, right thin')
            styleHeaderRight        = xlwt.easyxf('font: name Calibri, height 220; align: vertical center, horizontal center; pattern: pattern solid, fore_colour dark_blue; border: left thin, top thick, bottom thick, right thick')

            styleTitle              = xlwt.XFStyle()
            styleTitle.font         = fontTitle
            styleTitle.alignment    = alignmentCenter

            # styleDate = xlwt.XFStyle()
            # styleDate.num_format_str = 'D-MMM-YY'

            """     CREATE WORKBOOK         """
            wb = xlwt.Workbook()
            ws = wb.add_sheet('Daftar Donatur')

            ws.col(0).width  = 35 * 20
            ws.col(2).width  = 400 * 20
            ws.col(3).width  = 350 * 20
            ws.col(4).width  = 35 * 20
            ws.col(5).width  = 256 * 20

            ws.row(2).height_mismatch = True
            ws.row(2).height = 25 * 20
            ws.row(4).height_mismatch = True
            ws.row(4).height = 25 * 20

            ws.write_merge (1, 2, 1, 5, 'Daftar Donatur Program ' + program.nama, styleTitle)

            ws.write(4, 1, 'No', styleHeaderLeft)
            ws.write(4, 2, 'Nama', styleHeaderCenter)
            ws.write(4, 3, 'E - mail', styleHeaderCenter)
            ws.write_merge (4, 4, 4, 5, 'No. HP', styleHeaderRight)

            for i,donatur in enumerate(setDonatur):
                if i == len(setDonatur) - 1:
                    ws.write(5 + i, 1, i + 1, styleMainLeftBottom)
                    ws.write(5 + i, 2, donatur.nama, styleMainCenterBottom)
                    ws.write(5 + i, 3, donatur.email, styleMainCenterBottom)
                    ws.write(5 + i, 4, 0, styleMainLeftHPBottom)
                    ws.write(5 + i, 5, donatur.nohp, styleMainRightBottom)

                else:
                    ws.write(5 + i, 1, i + 1, styleMainLeft)
                    ws.write(5 + i, 2, donatur.nama, styleMainCenter)
                    ws.write(5 + i, 3, donatur.email, styleMainCenter)
                    ws.write(5 + i, 4, 0, styleMainLeftHP)
                    ws.write(5 + i, 5, donatur.nohp, styleMainRightHP)

            wb.save(response)

            return response

    else:

        return redirect('/admin/login')

def downloadWord(request, programId):

    if auth(request):
        if request.method == 'GET':

            program     = get_object_or_404(Program, pk=programId)
            setFoto     = program.fotoprogram_set.all()
            setBarang   = program.barang_set.all()
            setRekening = program.donasi_set.all()

            response = HttpResponse(content_type='application/ms-word')
            response['Content-Disposition'] = 'attachment; filename="program-' + program.nama + '.docx"'

            """     WRITING FILES           """
            document = Document()

            document.add_heading('Laporan Pertanggung Jawaban', 0)
            document.add_paragraph('Program ' + program.nama , style='Subtitle')

            document.add_heading('Deskripsi', 1)
            document.add_paragraph(program.deskripsi.strip())

            document.add_heading('Target', 1)
            document.add_paragraph(program.target.strip())

            document.add_heading('Kebutuhan', 1)
            if setBarang:
                for barang in setBarang:
                    string_barang = barang.nama
                    if barang.jumlah:
                        string_barang += " (Jumlah : " + barang.jumlah + ")"

                    document.add_paragraph(string_barang, style='ListBullet')
            else:
                l = document.add_paragraph("", style='ListBullet')
                l.add_run('Tidak ada').italic = True

            document.add_heading('Partner', 1)

            if program.logoPartner != None:
                document.add_picture( program.logoPartner, width=Inches(2.5))

            if program.partner != None:
                document.add_paragraph(program.partner.strip())


            document.add_heading('Rekening', 1)
            if setRekening:
                for r in setRekening:
                    # <strong>{{rek.rekening.bank}}</strong>, {{rek.rekening.noRekening}} a/n {{rek.rekening.nama}}
                    p = document.add_paragraph("", style='ListBullet')
                    p.add_run(r.rekening.bank).bold = True
                    p.add_run(", " + r.rekening.noRekening + " a/n " + r.rekening.nama)
                    p.add_run(' ( Deadline : ' + str(setRekening[0].deadline) + ' )').italic = True
            else:
                l = document.add_paragraph("", style='ListBullet')
                l.add_run('Tidak ada').italic = True
            # )

            for foto in setFoto:
                document.add_picture( foto.gambar, width=Inches(4))

            document.add_page_break()

            document.save(response)

            return response

    else:

        return redirect('/admin/login')
