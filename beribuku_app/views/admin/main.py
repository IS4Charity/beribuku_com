from beribuku_app.views.admin.login import *
from django.contrib.auth.decorators import login_required
#from django.core.mail import *
from django.shortcuts import get_object_or_404
from django.template import Context
from django.template.loader import get_template

from beribuku_app.views import validator


@login_required()
def home(request):
    donasi = Donasi.objects.all()
    donasi_selected = None
    list_donasi_belum_deadline = [];


    for d in donasi :
        if d.belum_lewat_deadline():
            list_donasi_belum_deadline.append(d)


    data = {
        'sumOfProgram': Program.objects.all().count(), 
        'sumOfDonatur': Donatur.objects.all().count(),
        'sumOfCurrentProgram': len(list_donasi_belum_deadline),
        'list_donasi_belum_deadline': list_donasi_belum_deadline,
        'admin_home_active' : 'active'
    }

    return render(request, 'admin/home.html', data)

def mail_lpj(request, programId):

    program         = get_object_or_404(Program, pk=programId)
    donasi          = get_object_or_404(Donasi, program=program)
    donatur         = Donatur.objects.filter(program=program)
    rekening        = donasi.rekening

    d               = Context({'program': program, 'host': '{{ host }}', 'donasi': donasi, 'rekening': rekening,
                        'nama_donatur':'{{ nama_donatur }}' } )

    template_html   = get_template('format_mail_lpj.html')
    html_content_temp    = template_html.render(d)
    email_subject   = "Laporan Pertanggung Jawaban Proyek " + "'" + program.nama + "'"

    data            = {'program': program, 'message': html_content_temp, 'subjek': email_subject, 'donatur': donatur, 'nama_donatur':'{{ nama_donatur }}', 
        'admin_proyek_active' : 'active'}

    if auth(request):
        if request.method == 'POST':
            email_list      = []
            email_list_clr  = []
            email_fail      = ''
            success_count   = 0
            try:
                
                email_subject   = request.POST.get('subjek')
                email_donatur   = request.POST.get('alamat_email')
                email_lainnya   = request.POST.get('email_lainnya')
                attach          = request.FILES['file']
               

                if(request.POST.get('kirim') == '1'):
                    email_all_donatur = []
                    for d in donatur:
                        email_all_donatur.append(d.email)
                    email_list = email_all_donatur
                else :    
                    if not validator.is_empty(email_donatur):
                        email_donatur_list = email_donatur.split(";")
                        email_list += email_donatur_list

                if not validator.is_empty(email_lainnya):
                    email_lainnya_list   = email_lainnya.split(";")
                    email_list          += email_lainnya_list

                txt_content     = request.POST.get('pesan')
                text_file       = open(settings.PROJECT_PATH + "/beribuku_app/template/temp.html", "w", encoding='utf-8')
                text_file.write(txt_content)
                text_file.close()


                for e in email_list:
                    if (not validator.is_empty(e)) and validator.is_email(e):
                        email_list_clr.append(e)

                for e in email_list_clr:
                    email   = []
                    e       = e.strip()
                    if (not validator.is_empty(e)) and validator.is_email(e):
                        email.append(e)

                        nama = ""
                        try:
                            donatur_p   = get_object_or_404(Donatur, email=e)
                            nama        = donatur_p.nama
                        except Exception as e:
                            print(e)

                        d_temp               = Context({'nama_donatur': nama,'program': program, 'host': 'http://localhost:8000', 'donasi': donasi, 'rekening': rekening})
                        template_html_temp   = get_template("temp.html")
                        html_content_temp    = template_html_temp.render(d_temp)
                        # msg                  = EmailMultiAlternatives(email_subject, html_content_temp, 'moh.afifun@ui.ac.id', email)
                        # msg.attach_alternative(html_content_temp, "text/html")
                        msg                  = EmailMessage(email_subject, html_content_temp, 'moh.afifun@ui.ac.id', email, headers={'From': 'beribuku@gmail.com'})
                        msg.content_subtype  = "html"

                        if attach != None:
                            msg.attach(attach.name, attach.read(), attach.content_type)
                            
                        print(email)
                        msg.send()
                        print('sukses')
                        success_count  += 1
                    else:
                        email_fail     += e + ','
                    print('kosong')

            except Exception as e:
                print(e)

            gagal = len(email_list_clr)-success_count

            print(str(success_count))
            print(str(gagal))
            print(str(len(email_list_clr)))
            print('fail :' +  email_fail)

            data = {'program': program, 'message': html_content_temp, 'subjek': email_subject, 'donatur': donatur, 'sukses': success_count, 'gagal': gagal, 'nama_donatur':'{{ nama_donatur }}',
        'admin_proyek_active' : 'active'}
            return render(request, 'admin/mail_lpj.html', data)

        return render(request, 'admin/mail_lpj.html', data)
    else :
        return redirect('/admin/mail/lpj/' + programId)

def mail_proposal(request, programId):

    program         = get_object_or_404(Program, pk=programId)
    donasi          = get_object_or_404(Donasi, program=program)
    donatur         = Donatur.objects.all()
    rekening        = donasi.rekening

    d               = Context({'program': program, 'host': '{{ host }}', 'donasi': donasi, 'rekening': rekening,
                        'nama_donatur':'{{ nama_donatur }}' } )

    template_html   = get_template('format_mail_proposal.html')
    html_content_temp    = template_html.render(d)
    email_subject   = "[Beribuku] Proyek Saat ini : " + "'" + program.nama + "'"

    data            = {'program': program, 'message': html_content_temp, 'subjek': email_subject, 'donatur': donatur, 'nama_donatur':'{{ nama_donatur }}',
        'admin_proyek_active' : 'active'}

    if auth(request):
        if request.method == 'POST':
            try:
                
                email_subject   = request.POST.get('subjek')
                email_donatur   = request.POST.get('alamat_email')
                email_lainnya   = request.POST.get('email_lainnya')
                email_list      = []
                email_list_clr  = []
                email_fail      = ''
                success_count   = 0

                if(request.POST.get('kirim') == '1'):
                    email_all_donatur = []
                    for d in donatur:
                        email_all_donatur.append(d.email)
                    email_list = email_all_donatur
                else :    
                    if not validator.is_empty(email_donatur):
                        email_donatur_list = email_donatur.split(";")
                        email_list += email_donatur_list

                if not validator.is_empty(email_lainnya):
                    email_lainnya_list   = email_lainnya.split(";")
                    email_list          += email_lainnya_list

                txt_content     = request.POST.get('pesan')
                text_file       = open(settings.PROJECT_PATH + "/beribuku_app/template/temp.html", "w", encoding='utf-8')
                text_file.write(txt_content)
                text_file.close()

                

                for e in email_list:
                    if (not validator.is_empty(e)) and validator.is_email(e):
                        email_list_clr.append(e)

                for e in email_list_clr:
                    email   = []
                    e       = e.strip()
                    if (not validator.is_empty(e)) and validator.is_email(e):
                        email.append(e)

                        nama = ""
                        try:
                            donatur_p   = get_object_or_404(Donatur, email=e)
                            nama        = donatur_p.nama
                        except Exception as e:
                            print(e)

                        d_temp               = Context({'nama_donatur': nama,'program': program, 'host': 'http://localhost:8000', 'donasi': donasi, 'rekening': rekening})
                        template_html_temp   = get_template("temp.html")
                        html_content_temp    = template_html_temp.render(d_temp)
                        # msg             = EmailMultiAlternatives(email_subject, html_content_temp, 'moh.afifun@ui.ac.id', email)
                        # msg.attach_alternative(html_content_temp, "text/html")
                        msg                  = EmailMessage(email_subject, html_content_temp, 'moh.afifun@ui.ac.id', email, headers={'From': 'beribuku@gmail.com'})
                        msg.content_subtype  = "html"
                        print(email)
                        msg.send()
                        print('sukses')
                        success_count  += 1
                    else:
                        email_fail     += e + ','
                    print('kosong')

            except Exception as e:
                print(e)

            gagal = len(email_list_clr)-success_count

            print(str(success_count))
            print(str(gagal))
            print(str(len(email_list_clr)))
            print('fail :' +  email_fail)

            data = {'program': program, 'message': html_content_temp, 'subjek': email_subject, 'donatur': donatur, 'sukses': success_count, 'gagal': gagal, 'nama_donatur':'{{ nama_donatur }}',
        'admin_proyek_active' : 'active'}
            return render(request, 'admin/mail_proposal.html', data)

        return render(request, 'admin/mail_proposal.html', data)
    else :
        return redirect('/admin/mail/proposal/' + programId)


