__author__ = 'afifun'
from django import template

from beribuku_app.models import FotoProgram

register = template.Library()


@register.filter()
def get_all_picture():
    all_foto = FotoProgram.objects.all().order_by('-program')
    return all_foto